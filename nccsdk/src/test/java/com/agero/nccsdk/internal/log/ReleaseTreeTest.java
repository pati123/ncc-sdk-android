package com.agero.nccsdk.internal.log;

import android.content.Context;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.TrackingContext;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.data.network.aws.model.LogPayload;
import com.agero.nccsdk.internal.log.config.LogConfig;
import com.agero.nccsdk.internal.common.util.AppUtils;
import com.agero.nccsdk.internal.common.util.Clock;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 12/7/17.
 */

@RunWith(PowerMockRunner.class)
public class ReleaseTreeTest {

    @Mock
    private DataManager mockDataManager;

    @Mock
    private KinesisClient mockKinesisClient;

    @Mock
    private Clock clock;

    @Mock
    private Context mockContext;

    @Test
    @PrepareForTest({NccSdk.class, DeviceUtils.class, AppUtils.class})
    public void log_saves_to_kinesis() throws Exception {
        int minPriorityToLog = LogLevel.DEBUG.getId();
        int priorityAboveMin = LogLevel.WARN.getId();

        LogConfig logConfig = new LogConfig(0);
        logConfig.setMinLogLevel(minPriorityToLog);
        ReleaseTree releaseTree = new ReleaseTree(logConfig, mockDataManager, mockKinesisClient, clock);

        long timeMillis = 12345678;
        PowerMockito.when(clock.currentTimeMillis()).thenReturn(timeMillis);

        String timeLocal = "localTimestamp";
        PowerMockito.when(clock.getLocalTimestamp(timeMillis)).thenReturn(timeLocal);

        String userId = "testUserId";
        PowerMockito.when(mockDataManager.getUserId()).thenReturn(userId);

        PowerMockito.mockStatic(NccSdk.class);
        PowerMockito.when(NccSdk.class, "getApplicationContext").thenReturn(mockContext);
        PowerMockito.when(mockContext.getPackageName()).thenReturn("package");

        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.when(DeviceUtils.class, "getDeviceName").thenReturn("deviceName");

        PowerMockito.mockStatic(AppUtils.class);
        PowerMockito.when(AppUtils.class, "getAppVersion", mockContext).thenReturn("1.0");

        TrackingContext trackingContext = new TrackingContext();
        trackingContext.putDouble("d", 1.0);
        trackingContext.putString("s", "string");
        trackingContext.putLong("l", 1234567890);
        trackingContext.putBoolean("b", true);
        trackingContext.putFloat("f", 1.234567f);
        trackingContext.putInt("i", 1);
        PowerMockito.when(mockDataManager.getTrackingContext()).thenReturn(trackingContext);

        String message = "test message";
        LogPayload logPayload = new LogPayload(priorityAboveMin, userId, message);
        logPayload.setTimeUtc(timeMillis);
        logPayload.setTimeLocal(timeLocal);
        logPayload.setMetadata(trackingContext.getData());

        releaseTree.log(priorityAboveMin, "tag", message, null);

        String expectedPayloadJson = new Gson().toJson(logPayload);

        verify(mockKinesisClient).save(expectedPayloadJson, BuildConfig.KINESIS_STREAM_LOGS);
    }

    @Test
    public void log_not_saved_to_kinesis() throws Exception {
        int minPriorityToLog = LogLevel.DEBUG.getId();
        int priorityBelowMin = LogLevel.VERBOSE.getId();

        LogConfig logConfig = new LogConfig(0);
        logConfig.setMinLogLevel(minPriorityToLog);
        ReleaseTree releaseTree = new ReleaseTree(logConfig, mockDataManager, mockKinesisClient, clock);

        releaseTree.log(priorityBelowMin, "tag", "test message", null);

        verifyZeroInteractions(mockKinesisClient);
    }
    
}
