package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccAccelerometerConfig extends NccAbstractNativeConfig {

    public NccAccelerometerConfig() {
        super();
    }

    public NccAccelerometerConfig(int samplingRate) {
        super(samplingRate);
    }
}
