package com.agero.nccsdk.adt;

import android.content.Context;

abstract class AdtAbstractState implements AdtState {

    final Context applicationContext;

    AdtAbstractState(Context context) {
        this.applicationContext = context;
    }

}
