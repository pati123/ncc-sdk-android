package com.agero.nccsdk.adt;

import android.Manifest;
import android.content.Context;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccPermissionsManager;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.config.SessionConfig;
import com.agero.nccsdk.adt.detection.domain.geofence.GeofenceManager;
import com.agero.nccsdk.adt.detection.start.algo.DrivingStartStrategy;
import com.agero.nccsdk.adt.detection.start.algo.MotionDrivingStartStrategy;
import com.agero.nccsdk.adt.detection.start.algo.WifiDrivingStartStrategy;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.domain.data.NccWifiData;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.maps.model.LatLng;

import java.util.Locale;

import javax.inject.Inject;

public class AdtMonitoringState extends AdtAbstractState {

    @Inject
    GeofenceManager geofenceManager;

    @Inject
    DataManager dataManager;

    @Inject
    SensorManager sensorManager;

    private DrivingStartStrategy<NccMotionData> motionCollectionStartStrategy;
    private DrivingStartStrategy<NccWifiData> wifiDrivingStartStrategy;
    private boolean motionSensorSubscribed = true;
    private boolean wifiSensorSubscribed = true;

    private final NccSensorListener motionActivityListener = new NccSensorListener() {
        @Override
        public void onDataChanged(NccSensorType sensorType, NccSensorData data) {
            motionCollectionStartStrategy.evaluate((NccMotionData) data);
        }
    };

    private final NccSensorListener wifiListener = (sensorType, data) -> wifiDrivingStartStrategy.evaluate((NccWifiData) data);

    AdtMonitoringState(Context context) {
        super(context);
    }

    @Override
    public void onEnter() {
        Timber.d("Entered AdtMonitoringState");
        NccSdk.getComponent().inject(this);
        motionCollectionStartStrategy = new MotionDrivingStartStrategy(applicationContext, getMotionConfidenceThreshold());
        wifiDrivingStartStrategy = new WifiDrivingStartStrategy(
                applicationContext,
                getSessionConfig(),
                dataManager
        );
        startTriggers();
    }

    @Override
    public void onExit() {
        Timber.d("Exited AdtMonitoringState");
        stopTriggers();
    }

    @Override
    public void startMonitoring(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    @Override
    public void stopMonitoring(StateMachine<AdtState> machine) {
        machine.setState(new AdtInactiveState(applicationContext));
    }

    @Override
    public void startDriving(StateMachine<AdtState> machine) {
        machine.setState(new AdtDrivingState(applicationContext));
    }

    @Override
    public void stopDriving(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    private int getMotionConfidenceThreshold() {
        SessionConfig sessionConfig;
        try {
            sessionConfig = (SessionConfig) dataManager.getSdkConfig(SdkConfigType.SESSION);
        } catch (NccException ne) {
            sessionConfig = new SessionConfig();
            Timber.w("Exception getting session config. Using default motionConfidenceThreshold: %d",
                    sessionConfig.getMotionConfidenceThreshold()
            );
        }

        return sessionConfig.getMotionConfidenceThreshold();
    }

    private SessionConfig getSessionConfig() {
        SessionConfig sessionConfig;
        try {
            sessionConfig = (SessionConfig) dataManager.getSdkConfig(SdkConfigType.SESSION);
        } catch (NccException ne) {
            sessionConfig = new SessionConfig();
            Timber.w("Exception getting session config. Using default wifiReconnectTimeThreshold and wifiDisconnectStartSessionDelay: %d, %d",
                    sessionConfig.getWifiReconnectTimeThreshold(),
                    sessionConfig.getWifiDisconnectStartSessionDelay()
            );
        }

        return sessionConfig;
    }

    private void startTriggers() {
        // Geofence
        createGeofenceIfNeeded();

        // Motion
        motionSensorSubscribed = subscribeSensor(NccSensorType.MOTION_ACTIVITY, motionActivityListener);

        // Wifi
        wifiSensorSubscribed = subscribeSensor(NccSensorType.WIFI, wifiListener);
    }

    private void stopTriggers() {
        deleteGeofence();

        if (motionSensorSubscribed) {
            unsubscribeSensor(NccSensorType.MOTION_ACTIVITY, motionActivityListener);
        }

        if (wifiSensorSubscribed) {
            unsubscribeSensor(NccSensorType.WIFI, wifiListener);
        }
    }

    private void createGeofenceIfNeeded() {
        if (geofenceManager.geofenceExists()) {
            LatLng latLng = geofenceManager.getGeofenceLatLng();
            Timber.v("Geofence found at %f, %f", latLng.latitude, latLng.longitude);
        } else {
            if (!NccPermissionsManager.isPermissionAllowed(Manifest.permission.ACCESS_FINE_LOCATION)) {
                Timber.w("Attempted to create a geofence but the location permission has not been allowed");
            } else if (!DeviceUtils.isLocationSettingOnWithHighestAccuracy(applicationContext)) {
                Timber.w("Attempted to create a geofence but the location setting is off or is not set to high accuracy");
            } else {
                geofenceManager.createGeofenceAtCurrentLocation();
            }
        }
    }

    private void deleteGeofence() {
        if (geofenceManager.geofenceExists()) {
            LatLng latLng = geofenceManager.getGeofenceLatLng();
            Timber.v(String.format(Locale.US, "Removing geofence at %f, %f", latLng.latitude, latLng.longitude));
            geofenceManager.deleteGeofence();
        }
    }

    private boolean subscribeSensor(NccSensorType sensorType, NccSensorListener sensorListener) {
        try {
            sensorManager.subscribeSensorListener(sensorType, sensorListener, dataManager.getConfig(sensorType));
        } catch (NccException ne) {
            Timber.e(ne, "Exception subscribing %s sensor", sensorType.getName());
            return false;
        }

        return true;
    }

    private void unsubscribeSensor(NccSensorType sensorType, NccSensorListener sensorListener) {
        try {
            sensorManager.unsubscribeSensorListener(sensorType, sensorListener);
        } catch (NccException ne) {
            Timber.e(ne, "Exception unsubscribing %s sensor", sensorType.getName());
        }
    }
}
