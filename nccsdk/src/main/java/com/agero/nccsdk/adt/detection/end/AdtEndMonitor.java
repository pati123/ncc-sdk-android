package com.agero.nccsdk.adt.detection.end;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.detection.end.algo.DrivingEndStrategy;
import com.agero.nccsdk.adt.detection.end.algo.LocationModeDrivingEndStrategy;
import com.agero.nccsdk.adt.detection.end.algo.LocationTimerDrivingEndStrategy;
import com.agero.nccsdk.adt.detection.end.algo.MotionDrivingEndStrategy;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccLocationSettingsData;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.ubi.collection.AbstractSensorCollector;
import com.agero.nccsdk.adt.config.SessionConfig;
import com.agero.nccsdk.internal.log.Timber;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by james hermida on 11/29/17.
 */

public class AdtEndMonitor extends AbstractSensorCollector {

    @Inject
    DataManager dataManager;

    @Inject
    SensorManager sensorManager;

    private final NccSensorType[] sensorTypes;

    private Map<NccSensorType, NccSensorListener> monitoringListeners;
    private List<NccSensorType> failedSubscriptionMonitoringSensors;

    private DrivingEndStrategy<NccLocationData> locationTimerDrivingEndStrategy;
    private DrivingEndStrategy<NccMotionData> motionDrivingEndStrategy;
    private DrivingEndStrategy<NccLocationSettingsData> locationModeDrivingEndStrategy;

    public AdtEndMonitor(Context context, NccSensorType[] sensorTypes) {
        super(context);
        this.sensorTypes = sensorTypes;
    }

    @Override
    public void start() {
        if (!isStarted()) {
            NccSdk.getComponent().inject(this);

            // Init collection end monitoring resources
            initMonitoringStrategies(applicationContext);
            initMonitoringListeners();
            subscribeMonitoringSensors();
            releaseFailedMonitoringResources();

            isStarted = true;
        }
    }

    @Override
    public void stop() {
        if (isStarted()) {
            releaseMonitoringStrategyResources();
            unsubscribeMonitoringSensors();

            isStarted = false;
        }
    }

    private void initMonitoringStrategies(Context context) {
        SessionConfig sessionConfig;
        try {
            sessionConfig = (SessionConfig) dataManager.getSdkConfig(SdkConfigType.SESSION);
        } catch (NccException ne) {
            Timber.w("Failed to get session config. Using defaults.", ne);
            sessionConfig = new SessionConfig();
        }

        // TODO generify
        locationTimerDrivingEndStrategy = new LocationTimerDrivingEndStrategy(context, sessionConfig.getTimeout(), sessionConfig.getLocationSpeedThreshold());
        motionDrivingEndStrategy = new MotionDrivingEndStrategy(context, sessionConfig.getMotionConfidenceThreshold());
        locationModeDrivingEndStrategy = new LocationModeDrivingEndStrategy(context);
    }

    private void initMonitoringListeners() {
        if (sensorTypes == null || sensorTypes.length == 0) {
            Timber.e("Cannot init collection end sensors listeners. mCollectionSensorTypes is null or empty");
        } else {
            monitoringListeners = new ArrayMap<>();

            // TODO generify
            monitoringListeners.put(NccSensorType.LOCATION, (sensorType, data) -> locationTimerDrivingEndStrategy.evaluate((NccLocationData) data));

            monitoringListeners.put(NccSensorType.MOTION_ACTIVITY, (sensorType, data) -> motionDrivingEndStrategy.evaluate((NccMotionData) data));

            monitoringListeners.put(NccSensorType.LOCATION_MODE, (sensorType, data) -> locationModeDrivingEndStrategy.evaluate((NccLocationSettingsData) data));
        }
    }

    private void subscribeMonitoringSensors() {
        if (monitoringListeners == null) {
            Timber.e("Cannot subscribe collection end sensors. monitoringListeners is null");
        } else {
            for (Map.Entry<NccSensorType, NccSensorListener> entry : monitoringListeners.entrySet()) {
                try {
                    sensorManager.subscribeSensorListener(entry.getKey(), entry.getValue(), dataManager.getConfig(entry.getKey()));
                } catch (NccException ne) {
                    Timber.e(ne, "Exception subscribing to collection end sensor %s", entry.getKey().getName());

                    // Keep track of sensor that had an exception during subscription
                    if (failedSubscriptionMonitoringSensors == null) {
                        failedSubscriptionMonitoringSensors = new ArrayList<>();
                    }
                    failedSubscriptionMonitoringSensors.add(entry.getKey());
                }
            }
        }
    }

    /**
     * Clean up resources for monitoring sensors that have failed subscription.
     */
    private void releaseFailedMonitoringResources() {
        if (failedSubscriptionMonitoringSensors != null) {
            releaseCollectionEndSensorListeners();
        }
    }

    private void releaseCollectionEndSensorListeners() {
        if (failedSubscriptionMonitoringSensors == null) {
            Timber.w("Cannot remove failed subscription collection end sensor listeners. failedSubscriptionMonitoringSensors is null");
        } else {
            for (NccSensorType sensorType : failedSubscriptionMonitoringSensors) {
                if (monitoringListeners.containsKey(sensorType)) {
                    monitoringListeners.remove(sensorType);
                }
            }
        }
    }

    private void releaseMonitoringStrategyResources() {
        if (locationTimerDrivingEndStrategy != null) {
            locationTimerDrivingEndStrategy.releaseResources();
        }

        if (motionDrivingEndStrategy != null) {
            motionDrivingEndStrategy.releaseResources();
        }

        if (locationModeDrivingEndStrategy != null) {
            locationModeDrivingEndStrategy.releaseResources();
        }
    }

    private void unsubscribeMonitoringSensors() {
        if (monitoringListeners == null) {
            Timber.e("Cannot unsubscribe collection end sensors. mSensorListeners is null");
        } else {
            for (Map.Entry<NccSensorType, NccSensorListener> entry : monitoringListeners.entrySet()) {
                try {
                    sensorManager.unsubscribeSensorListener(entry.getKey(), entry.getValue());
                } catch (NccException ne) {
                    Timber.e(ne, "Exception unsubscribing to collection end sensor %s", entry.getKey().getName());
                }
            }
        }
    }
}
