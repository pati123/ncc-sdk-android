package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccRotationVectorData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccRotationVectorMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccRotationVectorData rvd = (NccRotationVectorData) data;
        clearStringBuilder();
        sb.append("Q,");                                    // Code
        sb.append(rvd.getQuaternionX()); sb.append(",");    // Qx
        sb.append(rvd.getQuaternionY()); sb.append(",");    // Qy
        sb.append(rvd.getQuaternionZ()); sb.append(",");    // Qz
        sb.append(rvd.getQuaternionW()); sb.append(",");    // Qw
        sb.append("NA,NA,NA,NA,NA,");
        sb.append(rvd.getTimestamp()); sb.append(",");      // UTCTime
        sb.append(getReadableTimestamp(rvd.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
