package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 *
 * Refer to https://agero-mobility.atlassian.net/wiki/spaces/PP/pages/32669716/Combined+Sensor+File+Spec+v.1.2.1
 *
 */

public interface SensorDataMapper {

    String map(NccSensorData data);

}
