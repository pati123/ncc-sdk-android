package com.agero.nccsdk.internal.common.util;

/**
 * Created by james hermida on 12/7/17.
 */

public interface Clock {

    long currentTimeMillis();
    String getLocalTimestamp(long timeMillis);

}
