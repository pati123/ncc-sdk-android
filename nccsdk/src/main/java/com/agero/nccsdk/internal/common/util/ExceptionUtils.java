package com.agero.nccsdk.internal.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by james hermida on 8/23/17.
 */

public class ExceptionUtils {

    public static String getStacktraceString(Throwable t) {
        if (t == null) {
            return "";
        } else {
            // Don't replace this with Log.getStackTraceString() - it hides
            // UnknownHostException, which is not what we want.
            StringWriter sw = new StringWriter(256);
            PrintWriter pw = new PrintWriter(sw, false);
            t.printStackTrace(pw);
            pw.flush();
            return sw.toString();
        }
    }

}
