package com.agero.nccsdk.internal.data.network.aws.kinesis;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.di.qualifier.LbtDataPath;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.lbt.LbtAction;
import com.amazonaws.auth.AWSCredentialsProvider;

import java.io.File;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import static com.agero.nccsdk.lbt.LbtCollectionState.ACTION_LBT;

/**
 * Created by james hermida on 2/12/18.
 */

public class LbtKinesisClient extends AbstractKinesisClient {

    private final Executor executor;
    private final LbtActionReceiver lbtActionReceiver;

    @Inject
    public LbtKinesisClient(Context context, Executor executor, @LbtDataPath File directory, AWSCredentialsProvider credentialsProvider, AuthManager authManager) {
        super(directory, credentialsProvider, authManager);
        this.executor = executor;

        lbtActionReceiver = new LbtActionReceiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(lbtActionReceiver, new IntentFilter(ACTION_LBT));
    }

    @Override
    public void submit() {
        Timber.v("Submitting LBT data to stream. Bytes used: " + getDiskBytesUsed());
        super.submit();
    }

    public class LbtActionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(ACTION_LBT)) {
                LbtAction action = (LbtAction) intent.getSerializableExtra(ACTION_LBT);
                if (action == LbtAction.STOP) {
                    executor.execute(LbtKinesisClient.this::submit);
                }
            } else {
                Timber.w("Broadcast received with no ACTION_LBT extra");
            }
        }
    }
}
