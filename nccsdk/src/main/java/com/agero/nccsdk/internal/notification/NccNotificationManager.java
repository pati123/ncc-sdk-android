package com.agero.nccsdk.internal.notification;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.notification.model.NccNotification;

/**
 * Created by james hermida on 12/8/17.
 */

public class NccNotificationManager implements NotificationManager {

    private final String TAG = NccNotificationManager.class.getSimpleName();

    private final Context context;
    private final DataManager dataManager;

    public NccNotificationManager(Context context, DataManager dataManager) {
        this.context = context;
        this.dataManager = dataManager;
    }

    @Override
    public void setNotification(int id, @DrawableRes int drawableId, String contentTitle, String contextText) throws NccException {
        if (id == 0) {
            throw new NccException(TAG, "id must not 0", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (getDrawableById(drawableId) == null) {
            throw new NccException(TAG, "drawableId not found", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (StringUtils.isNullOrEmpty(contentTitle)) {
            throw new NccException(TAG, "contentTitle must not be null or empty", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (StringUtils.isNullOrEmpty(contextText)) {
            throw new NccException(TAG, "contextText must not be null or empty", NccExceptionErrorCode.INVALID_PARAMETER);
        } else {
            NccNotification notification = new NccNotification(id, drawableId, contentTitle, contextText);
            dataManager.saveNotification(notification);
        }
    }

    @Override
    public NccNotification getNotification(int id) {
        return dataManager.getNotification(String.valueOf(id));
    }

    private Drawable getDrawableById(@DrawableRes int drawableId) {
        return ContextCompat.getDrawable(context, drawableId);
    }
}
