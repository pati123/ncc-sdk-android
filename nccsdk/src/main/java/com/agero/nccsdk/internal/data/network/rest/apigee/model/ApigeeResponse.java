package com.agero.nccsdk.internal.data.network.rest.apigee.model;

public class ApigeeResponse {

    private final String message;
    private final int code;
    private final String description;

    private String identityPoolId;

    ApigeeResponse(String message, int code, String description) {
        this.message = message;
        this.code = code;
        this.description = description;
    }

    ApigeeResponse(String message, int code, String description, String identityPoolId) {
        this.message = message;
        this.code = code;
        this.description = description;
        this.identityPoolId = identityPoolId;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getIdentityPoolId() {
        return identityPoolId;
    }

    @Override
    public String toString() {
        return "ApigeeResponse{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", description='" + description + '\'' +
                ", identityPoolId='" + identityPoolId + '\'' +
                '}';
    }
}
