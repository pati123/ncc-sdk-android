package com.agero.nccsdk.internal.data;

import com.agero.nccsdk.internal.data.memory.ClientCache;
import com.agero.nccsdk.internal.data.preferences.ConfigManager;
import com.agero.nccsdk.internal.data.preferences.SharedPrefs;

/**
 * Created by james hermida on 10/4/17.
 */

public interface DataManager extends ConfigManager, SharedPrefs, ClientCache {

}
