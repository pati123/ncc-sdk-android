package com.agero.nccsdk.lbt;

import java.io.Serializable;

/**
 * Created by james hermida on 11/8/17.
 */

public enum LbtAction implements Serializable {

    START("Start"),

    STOP("Stop");

    private final String name;

    LbtAction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
