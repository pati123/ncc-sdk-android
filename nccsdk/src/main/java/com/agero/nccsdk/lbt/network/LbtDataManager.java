package com.agero.nccsdk.lbt.network;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.memory.ClientCache;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.data.network.aws.model.LbtPayload;
import com.agero.nccsdk.internal.data.preferences.ConfigManager;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.agero.nccsdk.internal.data.preferences.SharedPrefs;
import com.agero.nccsdk.internal.di.qualifier.LbtKinesisClient;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.lbt.LbtActionReceiver;
import com.agero.nccsdk.lbt.config.KinesisConfig;
import com.agero.nccsdk.lbt.util.DataUtils;
import com.agero.nccsdk.lbt.util.TimedOperation;
import com.google.gson.Gson;

import org.json.JSONException;

import java.util.List;

import javax.inject.Inject;

public class LbtDataManager implements LbtSyncManager, LbtActionReceiver.Callback {

    private final SharedPrefs sharedPrefs;
    private final ClientCache clientCache;
    private final TimedOperation kinesisUploadTimer;
    private final ConfigManager configManager;

    private final KinesisClient kinesisClient;

    private final Gson gson;

    @Inject
    LbtDataManager(@LbtKinesisClient KinesisClient sensorKinesisClient, NccSharedPrefs sharedPrefs, ClientCache clientCache,
                   TimedOperation kinesisUploadTimer, ConfigManager configManager) {
        kinesisClient = sensorKinesisClient;
        this.sharedPrefs = sharedPrefs;
        this.clientCache = clientCache;
        this.kinesisUploadTimer = kinesisUploadTimer;
        this.configManager = configManager;
        gson = new Gson();
    }

    @Override
    public void streamData(NccSensorType sensorType, List<? extends NccSensorData> sensorData) {
        if (sensorData == null) {
            Timber.e("streamData() - sensorData is null for %s", sensorType.getName());
        } else {
            try {
                LbtPayload payload = new LbtPayload(
                        sharedPrefs.getUserId(),
                        sensorType.getShortIdentifier(),
                        DataUtils.toJSONArrayString(sensorData) /* This is a workaround for Gson not being able to serialize polymorphic list collections within a class
                                                                 * Refer to https://github.com/google/gson/issues/170 */
                );
                if (clientCache.getTrackingContext() != null) {
                    payload.setMetadata(clientCache.getTrackingContext().getData());
                }
                String serializedPayload = gson.toJson(payload);
                streamData(serializedPayload, getKinesisStreamName(sensorType));
            } catch (JSONException je) {
                Timber.e(je, "Exception streaming data for %s: %s", sensorType.getName(), je.getMessage());
            }
        }
    }

    private void streamData(String data, String streamName) {
        if (StringUtils.isNullOrEmpty(data)) {
            Timber.e("streamData() - data is null or empty");
        } else {
            kinesisClient.save(data, streamName);
        }
    }

    private String getKinesisStreamName(NccSensorType sensorType) {
        switch (sensorType) {
            default:
                return BuildConfig.KINESIS_STREAM_LBT;
        }
    }

    @Override
    public void onStart() {
        try {
            long uploadInterval = ((KinesisConfig) configManager.getSdkConfig(SdkConfigType.KINESIS)).getUploadInterval();
            kinesisUploadTimer.start(uploadInterval);
        } catch (NccException e) {
            Timber.e(e, "Timer not started: %s", e.getMessage());
        }
    }

    @Override
    public void onStop() {
        kinesisUploadTimer.stop();
    }
}
